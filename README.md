# OpenML dataset: aloi

https://www.openml.org/d/42396

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Anderson Rocha and Siome Goldenstein.  
**Source**: Unknown - 2014  
**Please cite**: IEEE Transactions on Neural Networks and Learning Systems, 25(2):289-302, 2014  

Multiclass from binary: Expanding one-vs-all, one-vs-one and ECOC-based approaches. Dataset taken from LIBSVM: https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass.html

In this dataset version, the target attribute is fixed and is given as a nominal feature.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42396) of an [OpenML dataset](https://www.openml.org/d/42396). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42396/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42396/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42396/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

